const header = require('./header');
const footer = require('./footer');
const db = require('./db');

const getDoc = async function (type, x, y) {
    let ans = header.header;
    switch (type) {
        case 1:
            ans += await rep1(x, y);
            break;
        case 2:
            ans += await rep2(x, y);
            break;
        case 3:
            ans += await rep3(x, y);
            break;
        case 4:
            ans += await rep4(x, y);
            break;
        case 5:
            ans += await rep5(x, y);
            break;
        case 6:
            ans += await rep6(x, y);
            break;
        case 7:
            ans += await rep7(x, y);
            break;
        case 8:
            ans += await rep8(x, y);
            break;
        case 9:
            ans += await rep9(x, y);
            break;
        case 10:
            ans += await rep10(x, y);
            break;
        case 11:
            ans += await rep11(x, y);
            break;
        default:
            ans += '';
            break;
    }
    ans += footer.footer;
    return ans;
};
const getJSON = async function(type, x, y){
    let ans;
    switch (type) {
        case 1:
            ans = await _rep1(x, y);
            break;
        case 2:
            ans = await _rep2(x, y);
            break;
        case 3:
            ans = await _rep3(x, y);
            break;
        case 4:
            ans = await _rep4(x, y);
            break;
        case 5:
            ans = await _rep5(x, y);
            break;
        case 6:
            ans = await _rep6(x, y);
            break;
        case 7:
            ans = await _rep7(x, y);
            break;
        case 8:
            ans = await _rep8(x, y);
            break;
        case 9:
            ans = await _rep9(x, y);
            break;
        case 10:
            ans = await _rep10(x, y);
            break;
        case 11:
            ans = await _rep11(x, y);
            break;
        default:
            ans = [];
            break;
    }
    return ans;
};

let rep1 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 1</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Help Desk</th>\n' +
        '    <th>Punctuation</th>\n' +
        '  </tr>\n';

    let rows = await db.NormalQuery(
        'SELECT u.MAIL "mail", AVG(c.VALORATION) "average"\n' +
        'FROM USERR u inner join CONVERSATION c on u.USERRID = c.USERRHELPERID\n' +
        'GROUP BY u.MAIL\n' +
        'ORDER BY "average" DESC',[]);

    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].mail+'</td>\n' +
            '    <td>'+rows[i].average+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep2 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 2</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Help Desk</th>\n' +
        '    <th>Sex</th>\n' +
        '    <th>Birth Date</th>\n' +
        '  </tr>\n';

    let rows = await db.NormalQuery("SELECT u.mail \"mail\", u.birthdate \"birthdate\"\n" +
        "    FROM USERR u\n" +
        "    WHERE u.genderId = 2 AND u.userTypeId = 2 AND u.birthdate > '"+x+"-00-00 00:00:00'\n" +
        "    ORDER BY u.birthdate DESC",[]);

    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].mail+'</td>\n' +
            '    <td>Masculino</td>\n' +
            '    <td>'+rows[i].birthdate+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep3 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 3</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Admin</th>\n' +
        '    <th>Sex</th>\n' +
        '    <th>Birth Date</th>\n' +
        '  </tr>\n';

    let rows = await db.NormalQuery("SELECT u.mail \"mail\", u.birthdate \"birthdate\"\n" +
        "    FROM USERR u\n" +
        "    WHERE u.genderId = 1 AND u.userTypeId = 1 AND u.birthdate < '"+x+"-00-00 00:00:00'\n" +
        "    ORDER BY u.birthdate DESC",[]);

    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].mail+'</td>\n' +
            '    <td>Femenino</td>\n' +
            '    <td>'+rows[i].birthdate+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep4 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 4</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Client</th>\n' +
        '    <th>Gains</th>\n' +
        '  </tr>\n';

    let rows = await db.NormalQuery("SELECT u.mail \"mail\", u.GAINOBTAINED \"gain\"\n" +
        "FROM USERR u\n" +
        "WHERE u.STATUS = 0\n" +
        "ORDER BY u.GAINOBTAINED DESC",[]);
    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].mail+'</td>\n' +
            '    <td>'+rows[i].gain+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep5 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 5</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Product</th>\n' +
        '    <th>Punctuation</th>\n' +
        '  </tr>\n';
    let rows = await db.NormalQuery("SELECT p.NAME \"name\", AVG(c.VALORATIONID) \"valoration\"\n" +
        "FROM PRODUCT p inner join COMMENTT c on p.PRODUCTID = c.PRODUCTID\n" +
        "group by p.PRODUCTID, p.NAME\n" +
        "ORDER BY \"valoration\" DESC",[]);
    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].name+'</td>\n' +
            '    <td>'+rows[i].valoration+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep6 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 6</h1>\n' +
        '\t\t\t</header>\n' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep7 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 7</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>User</th>\n' +
        '    <th>Products Amount</th>\n' +
        '  </tr>\n';

    let rows = await db.NormalQuery("SELECT * FROM (SELECT u.mail \"mail\", SUM(p.amount) \"cantidad\"\n" +
        "FROM USERR u inner join PRODUCT p on u.userrId = p.userrId\n" +
        "WHERE p.status = 0\n" +
        "GROUP BY u.userrId, u.mail\n" +
        "ORDER BY \"cantidad\" DESC) WHERE ROWNUM <=3 ",[]);
    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].mail+'</td>\n' +
            '    <td>'+rows[i].cantidad+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep8 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 8</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Client</th>\n' +
        '    <th>Product Amount</th>\n' +
        '  </tr>\n';

    for (let i = 0; i < 10; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>Fernando</td>\n' +
            '    <td>50</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep9 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 9</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Product</th>\n' +
        '    <th>Comments</th>\n' +
        '    <th>Published Date</th>\n' +
        '  </tr>\n';

    let rows = await db.NormalQuery("\n" +
        "SELECT p.name \"name\", COUNT(c.commenttId) \"comentarios\" \n" +
        "FROM PRODUCT p inner join COMMENTT c on p.productId = c.productId\n" +
        "WHERE p.status = 0 AND c.publishdate > '"+x+" 00:00:00' AND c.PUBLISHDATE < '"+x+" 24:59:59'\n" +
        "GROUP BY c.productId, p.name\n" +
        "ORDER BY \"comentarios\" DESC\n",[]);
    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].name+'</td>\n' +
            '    <td>'+rows[i].comentarios+'</td>\n' +
            '    <td>'+x+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep10 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 10</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Product</th>\n' +
        '    <th>Amount</th>\n' +
        '  </tr>\n';

    let rows = await db.NormalQuery("SELECT p.name \"name\", p.amount \"amount\" \n" +
        "FROM PRODUCT p\n" +
        "WHERE p.status = 0 AND p.amount = "+x+"\n" +
        "ORDER BY p.name",[]);
    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].name+'</td>\n' +
            '    <td>'+x+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};
let rep11 = async function (x, y) {
    let ans = '';
    ans += '<article>\n' +
        '\t\t\t<header>\n' +
        '\t\t\t\t<h1>Report 11</h1>\n' +
        '\t\t\t</header>\n' +
        ' <table style="width:100%; font-family: Arial;">\n' +
        '  <tr style="text-align: left; background-color: dimgray; color: darkred">\n' +
        '    <th>No</th>\n' +
        '    <th>Product</th>\n' +
        '    <th>Punctuation</th>\n' +
        '  </tr>\n';

    let rows = await db.NormalQuery("SELECT * FROM  (SELECT p.name \"name\", AVG(c.VALORATIONID) \"valoration\"\n" +
        "FROM PRODUCT p inner join COMMENTT c on p.productId = c.productId\n" +
        "WHERE p.status = 0\n" +
        "GROUP BY p.productId, p.name\n" +
        "ORDER BY \"valoration\" ASC) WHERE ROWNUM <= 3\n" +
        "\n",[]);
    for (let i = 0; i < rows.length; i++) {
        ans +=
            '  <tr>\n' +
            '    <td>' + (i + 1) + '</td>\n' +
            '    <td>'+rows[i].name+'</td>\n' +
            '    <td>'+rows[i].valoration+'</td>\n' +
            '  </tr>\n';
    }

    ans +=
        '</table> ' +
        '\t\t</article>\n' +
        '\t\t\n';
    return ans;
};



let _rep1 = async function (x, y) {
    return await db.NormalQuery(
        'SELECT u.MAIL "mail", AVG(c.VALORATION) "average"\n' +
        'FROM USERR u inner join CONVERSATION c on u.USERRID = c.USERRHELPERID\n' +
        'GROUP BY u.MAIL\n' +
        'ORDER BY "average" DESC',[]);
};
let _rep2 = async function (x, y) {
    return await db.NormalQuery("SELECT u.mail \"mail\", u.birthdate \"birthdate\"\n" +
        "    FROM USERR u\n" +
        "    WHERE u.genderId = 2 AND u.userTypeId = 2 AND u.birthdate > '"+x+"-00-00 00:00:00'\n" +
        "    ORDER BY u.birthdate DESC",[]);
};
let _rep3 = async function (x, y) {
    return await db.NormalQuery("SELECT u.mail \"mail\", u.birthdate \"birthdate\"\n" +
        "    FROM USERR u\n" +
        "    WHERE u.genderId = 1 AND u.userTypeId = 1 AND u.birthdate < '"+x+"-00-00 00:00:00'\n" +
        "    ORDER BY u.birthdate DESC",[]);
};
let _rep4 = async function (x, y) {
    return await db.NormalQuery("SELECT u.mail \"mail\", u.GAINOBTAINED \"gain\"\n" +
        "FROM USERR u\n" +
        "WHERE u.STATUS = 0\n" +
        "ORDER BY u.GAINOBTAINED DESC",[]);
};
let _rep5 = async function (x, y) {
    return await db.NormalQuery("SELECT p.NAME \"name\", AVG(c.VALORATIONID) \"valoration\"\n" +
        "FROM PRODUCT p inner join COMMENTT c on p.PRODUCTID = c.PRODUCTID\n" +
        "group by p.PRODUCTID, p.NAME\n" +
        "ORDER BY \"valoration\" DESC",[]);
};
let _rep6 = async function (x, y) {
    return [];
};
let _rep7 = async function (x, y) {
    return await db.NormalQuery("SELECT * FROM (SELECT u.mail \"mail\", SUM(p.amount) \"cantidad\"\n" +
        "FROM USERR u inner join PRODUCT p on u.userrId = p.userrId\n" +
        "WHERE p.status = 0\n" +
        "GROUP BY u.userrId, u.mail\n" +
        "ORDER BY \"cantidad\" DESC) WHERE ROWNUM <=3 ",[]);
};
let _rep8 = async function (x, y) {
    return [];
};
let _rep9 = async function (x, y) {
    return await db.NormalQuery("\n" +
        "SELECT p.name \"name\", COUNT(c.commenttId) \"comentarios\" \n" +
        "FROM PRODUCT p inner join COMMENTT c on p.productId = c.productId\n" +
        "WHERE p.status = 0 AND c.publishdate > '"+x+" 00:00:00' AND c.PUBLISHDATE < '"+x+" 24:59:59'\n" +
        "GROUP BY c.productId, p.name\n" +
        "ORDER BY \"comentarios\" DESC\n",[]);
};
let _rep10 = async function (x, y) {
    return await db.NormalQuery("SELECT p.name \"name\", p.amount \"amount\" \n" +
        "FROM PRODUCT p\n" +
        "WHERE p.status = 0 AND p.amount = "+x+"\n" +
        "ORDER BY p.name",[]);
};
let _rep11 = async function (x, y) {
    return await db.NormalQuery("SELECT * FROM  (SELECT p.name \"name\", AVG(c.VALORATIONID) \"valoration\"\n" +
        "FROM PRODUCT p inner join COMMENTT c on p.productId = c.productId\n" +
        "WHERE p.status = 0\n" +
        "GROUP BY p.productId, p.name\n" +
        "ORDER BY \"valoration\" ASC) WHERE ROWNUM <= 3\n" +
        "\n",[]);
};


module.exports.getDoc = getDoc;
module.exports.getJSON = getJSON;
