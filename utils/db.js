const oracledb = require('oracledb');
const config = require('../config/config');

const NormalQuery = async function (query, args) {
    let con = await oracledb.getConnection(config.dbCredentials);
    /*
    *  query takes string query
    *  args takes values for the query
    * */
    let result = await con.execute(query, args, {outFormat: oracledb.OBJECT});
    con.close(
        function (err) {
            if (err) {
                console.error(err.message);
            }
        });
    return result.rows;
};

const Select = async function (table, columns, conditions, conditionsVals) {
    let con = await oracledb.getConnection(config.dbCredentials);
    /*
    *  query takes string query
    *  args takes values for the query
    * */
    let query = 'SELECT ';
    for (let i = 0; i < columns.length; i++) {
        query += columns[i] + '"' + columns[i] + '"';
        if (i < columns.length - 1)
            query += ",";
        query += " ";
    }
    query += "FROM " + table;
    if (conditions.length > 0) {
        query += " WHERE ";
        for (let i = 0; i < conditions.length; i++) {
            if (typeof conditionsVals[i] == "string") {
                query += conditions[i] + " = ':" + conditions[i] + "'";
            } else {
                query += conditions[i] + " = :" + conditions[i];
            }
            if (i < conditions.length - 1)
                query += " AND ";
        }
    }
    let result = await con.execute(query, conditionsVals, {outFormat: oracledb.OBJECT});
    con.close(
        function (err) {
            if (err) {
                console.error(err.message);
            }
        });
    return result.rows;
};

const Select2 = async function (table, columns, conditions, conditionsVals) {
    let con = await oracledb.getConnection(config.dbCredentials);
    /*
    *  query takes string query
    *  args takes values for the query
    * */
    let query = 'SELECT ';
    for (let i = 0; i < columns.length; i++) {
        query += columns[i] + '"' + columns[i] + '"';
        if (i < columns.length - 1)
            query += ",";
        query += " ";
    }
    query += "FROM " + table;
    if (conditions.length > 0) {
        query += " WHERE ";
        for (let i = 0; i < conditions.length; i++) {
            if (typeof conditionsVals[i] == "string") {
                query += conditions[i] + " = '" + conditionsVals[i] + "'";
            } else {
                query += conditions[i] + " = " + conditionsVals[i];
            }
            if (i < conditions.length - 1)
                query += " AND ";
        }
    }
    //console.log(query);
    let result = await con.execute(query, {}, {outFormat: oracledb.OBJECT});
    con.close(
        function (err) {
            if (err) {
                console.error(err.message);
            }
        });
    return result.rows;
};

const SelectCustum = async function (table, columns, columnsNames, conditions, conditionsVals, order) {
    let con = await oracledb.getConnection(config.dbCredentials);
    /*
    *  query takes string query
    *  args takes values for the query
    * */
    let query = 'SELECT ';
    for (let i = 0; i < columns.length; i++) {
        query += columns[i] + '"' + columnsNames[i] + '"';
        if (i < columns.length - 1)
            query += ",";
        query += " ";
    }
    query += "FROM " + table;
    if (conditions.length > 0) {
        query += " WHERE ";
        for (let i = 0; i < conditions.length; i++) {
            if (typeof conditionsVals[i] == "string") {
                query += conditions[i] + " = '" + conditionsVals[i] + "'";
            } else {
                query += conditions[i] + " = " + conditionsVals[i];
            }
            if (i < conditions.length - 1)
                query += " AND ";
        }
    }

    if(order.length > 0){
        query += " ORDER BY ";
        for(let i = 0; i< order.length; i++){
            query += order[i];
            if(i < order.length -1)
                query += ",";
            query += " ";
        }
    }
    let result = await con.execute(query, [], {outFormat: oracledb.OBJECT});
    con.close(
        function (err) {
            if (err) {
                console.error(err.message);
            }
        });
    return result.rows;
};

/**
 * @return {string}
 */
const Insert = async function (table, columns, values) {
    let con = await oracledb.getConnection(config.dbCredentials);
    /*
    *  query takes string query
    *  args takes values for the query
    * */
    let query = 'INSERT INTO ';
    query += table;
    query += " ( ";
    for (let i = 0; i < columns.length; i++) {
        query += columns[i];
        if (i < columns.length - 1)
            query += ",";
        query += " ";
    }
    query += ")";
    query += " VALUES ( ";
    for (let i = 0; i < columns.length; i++) {
        if (typeof values[i] == "string") {
            query += "'" + values[i] + "'";
        }else {
            query += + values[i];
        }
        if (i < values.length - 1)
            query += ",";
        query += " ";
    }
    query += " )";

    console.log(query);

    await con.execute(query, {}, {outFormat: oracledb.OBJECT});
    await con.execute("COMMIT", {}, {outFormat: oracledb.OBJECT});

    //console.log(result);

    //console.log(query);
    con.close(
        function (err) {
            if (err) {
                console.error(err.message);
            }
        });
    return query;
};


/**
 * @return {string}
 */
const Update = async function(table, columns, columnsVals, conditions, conditionsVals){
    let con = await oracledb.getConnection(config.dbCredentials);
    let query = 'UPDATE ';
    query += table;

    query += " SET ";
    for (let i = 0; i < columns.length; i++) {
        query += columns[i] + " = ";
        if (typeof columnsVals[i] == "string") {
            query += "'" + columnsVals[i] + "'";
        }else {
            query += + columnsVals[i];
        }
        if (i < columnsVals.length - 1)
            query += ",";
        query += " ";
    }
    if (conditions.length > 0) {
        query += " WHERE ";
        for (let i = 0; i < conditions.length; i++) {
            if (typeof conditionsVals[i] == "string") {
                query += conditions[i] + " = '" + conditionsVals[i] + "'";
            } else {
                query += conditions[i] + " = " + conditionsVals[i];
            }
            if (i < conditions.length - 1)
                query += " AND ";
        }
    }

    console.log(query);

    await con.execute(query, {}, {outFormat: oracledb.OBJECT});
    await con.execute("COMMIT", {}, {outFormat: oracledb.OBJECT});

    con.close(
        function (err) {
            if (err) {
                console.error(err.message);
            }
        });
    return 1;
};

const getProducts = async function(category){
    let con = await oracledb.getConnection(config.dbCredentials);
    let query = 'with descendants(CATEGORYFATHERID, DESCRIPTION, CATEGORYID) as\n' +
        '       (select CATEGORYFATHERID, DESCRIPTION, CATEGORYID\n' +
        '        from CATEGORY\n' +
        '        union all\n' +
        '        select d.CATEGORYFATHERID, d.DESCRIPTION, s.CATEGORYID\n' +
        '        from descendants d\n' +
        '               join CATEGORY s\n' +
        '                    on d.CATEGORYID = s.CATEGORYFATHERID\n' +
        '       )\n' +
        '\n' +
        'SELECT ' +
        'productid "productId", ' +
        'categoryid "categoryId", ' +
        'userrid "userId", ' +
        'photo "photo", ' +
        'description "description", ' +
        'price "price", ' +
        'publishdate "publishdate", ' +
        'amount "amount", ' +
        'status "status", ' +
        'name "name", ' +
        'code "code"' +
        ', COUNT(PRODUCTID)\n' +
        'FROM (SELECT p.*\n' +
        '      FROM PRODUCT p\n' +
        '             inner join descendants d on p.CATEGORYID = d.CATEGORYID\n' +
        '             inner join descendants d2 on p.CATEGORYID = d2.CATEGORYFATHERID\n' +
        '      WHERE d.CATEGORYFATHERID = '+category+'\n' +
        '         OR d2.CATEGORYID = '+category+'\n' +
        '         OR p.CATEGORYID = '+category+') products\n' +
        'GROUP BY productid, categoryid, userrid, photo, description, price, publishdate, amount, status, name, code';

    let result = await con.execute(query, [], {outFormat: oracledb.OBJECT});
    con.close(
        function (err) {
            if (err) {
                console.error(err.message);
            }
        });
    return result.rows;
};

const getHelper = async function(){
    let con = await oracledb.getConnection(config.dbCredentials);
    let query = 'SELECT o.*\n' +
        'FROM (SELECT u.USERRID "userId", count(c.USERRHELPERID) "conversations"\n' +
        'FROM USERR u left join CONVERSATION c on u.USERRID = c.USERRHELPERID\n' +
        'where u.STATUS = 0 AND u.USERTYPEID = 2\n' +
        'group by u.USERRID\n' +
        'order by "conversations" asc) o\n' +
        'WHERE ROWNUM = 1';

    let result = await con.execute(query, [], {outFormat: oracledb.OBJECT});
    con.close(
        function (err) {
            if (err) {
                console.error(err.message);
            }
        });
    return result.rows;
};

module.exports.NormalQuery = NormalQuery;
module.exports.Select = Select;
module.exports.Select2 = Select2;
module.exports.SelectCustum = SelectCustum;
module.exports.Insert = Insert;
module.exports.Update = Update;
module.exports.getProducts = getProducts;
module.exports.getHelper = getHelper;
