const GetUser = function () {
    let user = {
        type: 0,
        amount: 0.0,
        validateCode: 'adasdañsdk'
    };
    const min = 3;
    const max = 8;
    user.type = Math.floor(Math.random() * (+max - +min)) + +min;

    switch (user.type) {
        case 3:
            user.amount = 50000.00;
            break;
        case 4:
            user.amount = 25000.00;
            break;
        case 5:
            user.amount = 10000.00;
            break;
        case 6:
            user.amount = 5000.00;
            break;
        case 7:
            user.amount = 1000.00;
            break;
    }

    user.validateCode = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    return user;
};

function randomString(length, chars) {
    let result = '';
    for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}


module.exports.GetUser = GetUser;

