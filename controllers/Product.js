'use strict';

var url = require('url');


var Product = require('./ProductService');


module.exports.addProduct = function addProduct (req, res, next) {
  Product.addProduct(req.swagger.params, res, next);
};

module.exports.addProductP = function addProductP (req, res, next) {
  Product.addProductP(req.swagger.params, res, next);
};

module.exports.allProducts = function allProducts (req, res, next) {
  Product.allProducts(req.swagger.params, res, next);
};

module.exports.allProductss = function allProductss (req, res, next) {
  Product.allProductss(req.swagger.params, res, next);
};

module.exports.deleteProductById = function deleteProductById (req, res, next) {
  Product.deleteProductById(req.swagger.params, res, next);
};

module.exports.getProductById = function getProductById (req, res, next) {
  Product.getProductById(req.swagger.params, res, next);
};

module.exports.updateProduct = function updateProduct (req, res, next) {
  Product.updateProduct(req.swagger.params, res, next);
};
