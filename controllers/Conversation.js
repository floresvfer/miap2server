'use strict';

var url = require('url');


var Conversation = require('./ConversationService');


module.exports.addConversation = function addConversation (req, res, next) {
  Conversation.addConversation(req.swagger.params, res, next);
};

module.exports.addConversationP = function addConversationP (req, res, next) {
  Conversation.addConversationP(req.swagger.params, res, next);
};

module.exports.allConversations = function allConversations (req, res, next) {
  Conversation.allConversations(req.swagger.params, res, next);
};

module.exports.allConversationsStatus = function allConversationsStatus (req, res, next) {
  Conversation.allConversationsStatus(req.swagger.params, res, next);
};

module.exports.deleteConversationById = function deleteConversationById (req, res, next) {
  Conversation.deleteConversationById(req.swagger.params, res, next);
};

module.exports.getConversationById = function getConversationById (req, res, next) {
  Conversation.getConversationById(req.swagger.params, res, next);
};

module.exports.getConversationByUserId = function getConversationByUserId (req, res, next) {
  Conversation.getConversationByUserId(req.swagger.params, res, next);
};

module.exports.updateConversation = function updateConversation (req, res, next) {
  Conversation.updateConversation(req.swagger.params, res, next);
};
