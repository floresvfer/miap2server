'use strict';

var url = require('url');


var CartDetail = require('./CartDetailService');


module.exports.addCartDetail = function addCartDetail (req, res, next) {
  CartDetail.addCartDetail(req.swagger.params, res, next);
};

module.exports.addCartDetailP = function addCartDetailP (req, res, next) {
  CartDetail.addCartDetailP(req.swagger.params, res, next);
};

module.exports.allCartDetails = function allCartDetails (req, res, next) {
  CartDetail.allCartDetails(req.swagger.params, res, next);
};

module.exports.deleteCartDetailById = function deleteCartDetailById (req, res, next) {
  CartDetail.deleteCartDetailById(req.swagger.params, res, next);
};

module.exports.getCartDetailById = function getCartDetailById (req, res, next) {
  CartDetail.getCartDetailById(req.swagger.params, res, next);
};

module.exports.updateCartDetail = function updateCartDetail (req, res, next) {
  CartDetail.updateCartDetail(req.swagger.params, res, next);
};
