'use strict';

var url = require('url');


var Comment = require('./CommentService');


module.exports.addComment = function addComment (req, res, next) {
  Comment.addComment(req.swagger.params, res, next);
};

module.exports.addCommentP = function addCommentP (req, res, next) {
  Comment.addCommentP(req.swagger.params, res, next);
};

module.exports.allComments = function allComments (req, res, next) {
  Comment.allComments(req.swagger.params, res, next);
};

module.exports.deleteCommentById = function deleteCommentById (req, res, next) {
  Comment.deleteCommentById(req.swagger.params, res, next);
};

module.exports.getCommentById = function getCommentById (req, res, next) {
  Comment.getCommentById(req.swagger.params, res, next);
};

module.exports.updateComment = function updateComment (req, res, next) {
  Comment.updateComment(req.swagger.params, res, next);
};
