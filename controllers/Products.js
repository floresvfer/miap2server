'use strict';

var url = require('url');


var Products = require('./ProductsService');


module.exports.getProductsByUserId = function getProductsByUserId (req, res, next) {
  Products.getProductsByUserId(req.swagger.params, res, next);
};
