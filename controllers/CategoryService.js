'use strict';
const db = require('../utils/db');


exports.addCategory = function(args, res, next) {
  /**
   * parameters expected in the args:
  * category (Category)
  **/
  // no response value expected for this operation
  res.end();
}

exports.addCategoryP = function(args, res, next) {
  /**
   * parameters expected in the args:
  * category (Category)
  **/
  // no response value expected for this operation
  res.end();
}

exports.allCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * status (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "description" : "categoria",
  "categoryId" : 1,
  "categoryFather" : 2,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.delCategoryById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * categoryId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "description" : "categoria",
  "categoryId" : 1,
  "categoryFather" : 2,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.getCategoryById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * categoryId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "description" : "categoria",
  "categoryId" : 1,
  "categoryFather" : 2,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.getCategoryTree = async function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    var examples = {};
  examples['application/json'] = {
  "description" : "categoria",
  "categoryId" : 1,
  "categoryFather" : 2,
  "status" : 0
};

  let ans = await db.SelectCustum('category',
      [
        'categoryId',
        'categoryFatherId',
        'description',
        'status'
      ],
      [
        'categoryId',
        'categoryFatherId',
        'description',
        'status'
      ],
      ['status'],
      [0],
      []);


  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));
}

exports.putCategory = function(args, res, next) {
  /**
   * parameters expected in the args:
  * category (Category)
  **/
  // no response value expected for this operation
  res.end();
}

