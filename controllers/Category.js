'use strict';

var url = require('url');


var Category = require('./CategoryService');


module.exports.addCategory = function addCategory (req, res, next) {
  Category.addCategory(req.swagger.params, res, next);
};

module.exports.addCategoryP = function addCategoryP (req, res, next) {
  Category.addCategoryP(req.swagger.params, res, next);
};

module.exports.allCategories = function allCategories (req, res, next) {
  Category.allCategories(req.swagger.params, res, next);
};

module.exports.delCategoryById = function delCategoryById (req, res, next) {
  Category.delCategoryById(req.swagger.params, res, next);
};

module.exports.getCategoryById = function getCategoryById (req, res, next) {
  Category.getCategoryById(req.swagger.params, res, next);
};

module.exports.getCategoryTree = function getCategoryTree (req, res, next) {
  Category.getCategoryTree(req.swagger.params, res, next);
};

module.exports.putCategory = function putCategory (req, res, next) {
  Category.putCategory(req.swagger.params, res, next);
};
