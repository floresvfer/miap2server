'use strict';

var url = require('url');


var Gender = require('./GenderService');


module.exports.allGenders = function allGenders (req, res, next) {
  Gender.allGenders(req.swagger.params, res, next);
};

module.exports.getGenderById = function getGenderById (req, res, next) {
  Gender.getGenderById(req.swagger.params, res, next);
};
