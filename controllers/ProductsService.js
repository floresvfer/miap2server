'use strict';
const db = require('../utils/db');


exports.getProductsByUserId = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "amount" : 10,
  "code" : "Y89YY3RRR",
  "productId" : 1,
  "price" : 50.0,
  "name" : "nombre del producto",
  "photo" : "photo.png",
  "description" : "descripcion del producto blah blah blah blah blah blah",
  "publisdate" : "2019-04-19",
  "category" : {
    "description" : "categoria",
    "categoryId" : 1,
    "categoryFather" : 2,
    "status" : 0
  },
  "user" : {
    "gainObtained" : 400.0,
    "birthdate" : "2018-01-25",
    "address" : "29 av, 26-12 Z7",
    "availableCredit" : 5000.0,
    "gender" : {
      "genderId" : 0,
      "description" : "Masculino",
      "status" : 0
    },
    "mail" : "mymail@gmail.com",
    "photo" : "/home/photos/photo.jpg",
    "validateCode" : "f4MtvVAfXO2nnZZCeEnTGDpImEJ5MqVA",
    "registerdate" : "2018-01-25",
    "userId" : 1,
    "lastname" : "flores",
    "password" : "pass",
    "phone" : "54252919",
    "name" : "fer",
    "userType" : {
      "userTypeId" : 0,
      "description" : "admin",
      "status" : 0
    },
    "status" : 0
  },
  "status" : 0
};
  let ans = await db.SelectCustum('product p inner join category c on p.categoryId = c.categoryId',
      [
        'p.productId',
        'p.categoryId',
        'p.userrId',
        'p.photo',
        'p.description',
        'p.price',
        'p.publishdate',
        'p.amount',
        'p.status',
        'p.name',
        'p.code',
        'c.description'
      ],
      [
        'productId',
        'categoryId',
        'userrId',
        'photo',
        'description',
        'price',
        'publishdate',
        'amount',
        'status',
        'name',
        'code',
        'categoryDesc'
      ],
      ['p.userrId'],
      [args.userId.value],
      ['p.publishdate']);

  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));
}

