'use strict';

var url = require('url');


var Report = require('./ReportService');


module.exports.getReport = function getReport (req, res, next) {
  Report.getReport(req.swagger.params, res, next);
};

module.exports.getReport2 = function getReport2 (req, res, next) {
  Report.getReport2(req.swagger.params, res, next);
};
