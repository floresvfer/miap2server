'use strict';
const db = require('../utils/db');

exports.addComment = function (args, res, next) {
    /**
     * parameters expected in the args:
     * comment (Comment)
     **/
    // no response value expected for this operation
    res.end();
}

exports.addCommentP = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * comment (Comment)
     **/
    // no response value expected for this operation
    let comment =  args.comment.value;
    await db.Insert('commentt',
        [
            'userrId',
            'productId',
            'valorationId',
            'publishdate',
            'title',
            'description',
            'status'
        ],
        [
            comment.userId,
            comment.productId,
            comment.valoration,
            comment.publishdate,
            comment.title,
            comment.description,
            0
        ]
    );
    res.setHeader('Content-Type', 'application/json');
    res.end();
}

exports.allComments = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * productId (Long)
     **/
    var examples = {};
    examples['application/json'] = {
        "valoration": 5,
        "productId": 1,
        "commentId": 1,
        "publishdate": "2019-04-19",
        "description": "blah blah blah blah blah blha.....",
        "title": "mi titulo",
        "userId": 1,
        "status": 0
    };
    let ans = await
        db.SelectCustum('commentt c inner join userr u on c.userrId = u.userrId',
            ['c.commenttId',
                'c.commenttFatherId',
                'c.userrId',
                'c.valorationId',
                'c.publishdate',
                'c.title',
                'c.description',
                'u.mail',
                'u.photo'
            ],
            ['commenttId',
                'commenttFatherId',
                'userrId',
                'valorationId',
                'publishdate',
                'title',
                'description',
                'userMail',
                'userPhoto'],
            ['c.status', 'c.productId'],
            [0, args.productId.value],
            ['c.publishdate']);

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));

}

exports.deleteCommentById = function (args, res, next) {
    /**
     * parameters expected in the args:
     * commentId (Long)
     **/
    // no response value expected for this operation
    res.end();
}

exports.getCommentById = function (args, res, next) {
    /**
     * parameters expected in the args:
     * commentId (Long)
     **/
    var examples = {};
    examples['application/json'] = {
        "valoration": 5,
        "productId": 1,
        "commentId": 1,
        "publishdate": "2019-04-19",
        "description": "blah blah blah blah blah blha.....",
        "title": "mi titulo",
        "userId": 1,
        "status": 0
    };
    if (Object.keys(examples).length > 0) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
    } else {
        res.end();
    }

}

exports.updateComment = function (args, res, next) {
    /**
     * parameters expected in the args:
     * comment (Comment)
     **/
    // no response value expected for this operation
    res.end();
}

