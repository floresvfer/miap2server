'use strict';

var url = require('url');


var Valoration = require('./ValorationService');


module.exports.allValorations = function allValorations (req, res, next) {
  Valoration.allValorations(req.swagger.params, res, next);
};

module.exports.getValorationById = function getValorationById (req, res, next) {
  Valoration.getValorationById(req.swagger.params, res, next);
};
