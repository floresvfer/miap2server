'use strict';

exports.addPurchaseDetail = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchaseDetail (PurchaseDetail)
  **/
  // no response value expected for this operation
  res.end();
}

exports.addPurchaseDetailP = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchaseDetail (PurchaseDetail)
  **/
  // no response value expected for this operation
  res.end();
}

exports.allPurchaseDetails = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchaseId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "amount" : 1,
  "productId" : 1,
  "purchaseDetailId" : 1,
  "purchaseId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.deletePurchaseDetailById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchaseDetailId (Long)
  **/
  // no response value expected for this operation
  res.end();
}

exports.getPurchaseDetailById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchaseDetailId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "amount" : 1,
  "productId" : 1,
  "purchaseDetailId" : 1,
  "purchaseId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.updatePurchaseDetail = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchaseDetail (PurchaseDetail)
  **/
  // no response value expected for this operation
  res.end();
}

