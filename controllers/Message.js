'use strict';

var url = require('url');


var Message = require('./MessageService');


module.exports.addMessage = function addMessage (req, res, next) {
  Message.addMessage(req.swagger.params, res, next);
};

module.exports.addMessageP = function addMessageP (req, res, next) {
  Message.addMessageP(req.swagger.params, res, next);
};

module.exports.allMessages = function allMessages (req, res, next) {
  Message.allMessages(req.swagger.params, res, next);
};

module.exports.deleteMessageById = function deleteMessageById (req, res, next) {
  Message.deleteMessageById(req.swagger.params, res, next);
};

module.exports.getMessageById = function getMessageById (req, res, next) {
  Message.getMessageById(req.swagger.params, res, next);
};

module.exports.updateMessage = function updateMessage (req, res, next) {
  Message.updateMessage(req.swagger.params, res, next);
};
