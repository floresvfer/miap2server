'use strict';

let usrGen = require('../utils/userGenerator');
let db = require('../utils/db');
let mail = require('../utils/mail');


exports.addUser = function (args, res, next) {
    /**
     * parameters expected in the args:
     * user (User)
     **/
    // no response value expected for this operation
    res.end();
}

exports.addUserP = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * user (User)
     **/
        // no response value expected for this operation
    let user = args.user.value;
    let props = usrGen.GetUser();
    user.userType.userTypeId = props.type;
    user.availableCredit = props.amount;
    user.validateCode = props.validateCode;
    await db.Insert('userr',
        [
            'gainObtained',
            'birthdate',
            'address',
            'availableCredit',
            'genderId',
            'mail',
            'photo',
            'validateCode',
            'registerdate',
            'lastname',
            'password',
            'phone',
            'name',
            'userTypeId',
            'status'
        ],
        [
            user.gainObtained,
            user.birthdate,
            user.address,
            user.availableCredit,
            user.gender.genderId,
            user.mail,
            'assets/images/user.png',
            user.validateCode,
            user.registerdate,
            user.lastname,
            user.password,
            user.phone,
            user.name,
            user.userType.userTypeId,
            1
        ]
    );

    mail.SendMail(user.mail, user.validateCode);

    res.setHeader('Content-Type', 'application/json');
    res.end();
}

exports.allUsers = async function (args, res, next) {
    /**
     * parameters expected in the args:
     **/
    let ans = await db.SelectCustum('userr u inner join userType ut on u.userTypeId = ut.userTypeId',
        [
            'u.userrId',
            'u.gainObtained',
            'u.birthdate',
            'u.address',
            'u.availableCredit',
            'u.genderId',
            'u.mail',
            'u.photo',
            'u.validateCode',
            'u.registerdate',
            'u.lastname',
            'u.password',
            'u.phone',
            'u.name',
            'u.userTypeId',
            'u.status',
            'ut.description'
        ],
        [
            'userrId',
            'gainObtained',
            'birthdate',
            'address',
            'availableCredit',
            'genderId',
            'mail',
            'photo',
            'validateCode',
            'registerdate',
            'lastname',
            'password',
            'phone',
            'name',
            'userTypeId',
            'status',
            'userTypeDesc'
        ],
        [],
        [],
        ['u.userTypeId', 'u.status']);


    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));

}

exports.allUserss = function (args, res, next) {
    /**
     * parameters expected in the args:
     * status (Long)
     **/
    var examples = {};
    examples['application/json'] = {
        "gainObtained": 400.0,
        "birthdate": "2018-01-25",
        "address": "29 av, 26-12 Z7",
        "availableCredit": 5000.0,
        "gender": {
            "genderId": 0,
            "description": "Masculino",
            "status": 0
        },
        "mail": "mymail@gmail.com",
        "photo": "/home/photos/photo.jpg",
        "validateCode": "f4MtvVAfXO2nnZZCeEnTGDpImEJ5MqVA",
        "registerdate": "2018-01-25",
        "userId": 1,
        "lastname": "flores",
        "password": "pass",
        "phone": "54252919",
        "name": "fer",
        "userType": {
            "userTypeId": 0,
            "description": "admin",
            "status": 0
        },
        "status": 0
    };
    if (Object.keys(examples).length > 0) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
    } else {
        res.end();
    }

}

exports.deleteUserById = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * userId (Long)
     **/
        // no response value expected for this operation
    let ans = await db.Update('userr',
        ['status'],
        [2],
        ['userrId'],
        [args.userId.value]);

    res.end();
}

exports.getUserById = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * userId (Long)
     **/

    let ans = await db.SelectCustum(
        'userr u inner join userType ut on u.userTypeId = ut.userTypeId' +
        ' inner join gender g on u.genderId = g.genderId',
        [
            'u.userrId',
            'u.gainObtained',
            'u.birthdate',
            'u.address',
            'u.availableCredit',
            'u.genderId',
            'u.mail',
            'u.photo',
            'u.validateCode',
            'u.registerdate',
            'u.lastname',
            'u.password',
            'u.phone',
            'u.name',
            'u.userTypeId',
            'u.status',
            'ut.description',
            'ut.status',
            'g.description',
            'g.status'
        ],
        [
            'userrId',
            'gainObtained',
            'birthdate',
            'address',
            'availableCredit',
            'genderId',
            'mail',
            'photo',
            'validateCode',
            'registerdate',
            'lastname',
            'password',
            'phone',
            'name',
            'userTypeId',
            'status',
            'userTypeDesc',
            'userTypeStatus',
            'genderDesc',
            'genderStatus'
        ],
        ['userrId'],
        [args.userId.value],
        ['u.userTypeId']);

    ans = ans[0];

    let examples = {};
    examples['application/json'] = {
        "gainObtained": ans.gainObtained,
        "birthdate": ans.birthdate,
        "address": ans.address,
        "availableCredit": ans.availableCredit,
        "gender": {
            "genderId": ans.genderId,
            "description": ans.genderDesc,
            "status": ans.genderStatus
        },
        "mail": ans.mail,
        "photo": ans.photo,
        "validateCode": ans.validateCode,
        "registerdate": ans.registerdate,
        "userId": ans.userrId,
        "lastname": ans.lastname,
        "password": ans.password,
        "phone": ans.phone,
        "name": ans.name,
        "userType": {
            "userTypeId": ans.userTypeId,
            "description": ans.userTypeDesc,
            "status": ans.userTypeStatus
        },
        "status": 0
    };
    if (Object.keys(examples).length > 0) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
    } else {
        res.end();
    }

}

exports.logIn = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * mail (String)
     * pwd (String)
     **/

    let ans = await db.Select2('userr',
        [
            'genderId',
            'userrId',
            'userTypeId',
            'photo',
            'mail',
            'name'
        ], [
            'mail',
            'password',
            'status'
        ], [
            args.mail.value,
            args.pwd.value,
            0
        ]);

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));

}

exports.putUserP = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * user (User)
     **/
        // no response value expected for this operation
    let user = args.user.value;
    let ans = await db.Update('userr',
        ['name',
            'lastname',
            'genderId',
            'phone',
            'birthdate',
            'address',
            'password'],
        [user.name,
        user.lastname,
        user.gender.genderId,
        user.phone,
        user.birthdate,
        user.address,
        user.password],
        ['userrId'],
        [user.userId]);

    res.end();
}

exports.validateUser = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * validateCode (String)
     **/
        // no response value expected for this operation

    let ans = await db.Select2('userr',
        ['status'],
        ['validateCode'],
        [args.validateCode.value]);
    let status;
    if (ans.length > 0) {

        await db.Update('userr',
            ['status'],
            [0],
            ['validateCode'],
            [args.validateCode.value]);

        status = {status: 1};
    } else {
        status = {status: 0};
    }
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(
        status || {},
        null,
        2));
}

