'use strict';
const db = require('../utils/db');

exports.addMessage = function (args, res, next) {
    /**
     * parameters expected in the args:
     * message (Message)
     **/
    // no response value expected for this operation
    res.end();
}

exports.addMessageP = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * message (Message)
     **/
    // no response value expected for this operation
    let message =  args.message.value;
    await db.Insert('message',
        [
            'userrId',
            'conversationId',
            'description',
            'senddate',
            'status'
        ],
        [
            message.userId,
            message.conversationId,
            message.description,
            message.senddate,
            0
        ]
    );
    res.setHeader('Content-Type', 'application/json');
    res.end();
}

exports.allMessages = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * conversationId (Long)
     **/


    var examples = {};
    examples['application/json'] = {
        "conversationId": 1,
        "senddate": "aeiou",
        "messageId": 1,
        "description": "aeiou",
        "userId": 27,
        "status": 0
    };
    let ans = await db.SelectCustum('message m inner join userr u on m.userrId = u.userrId',
        [
            'm.messageId',
            'm.userrId',
            'm.conversationId',
            'm.description',
            'm.senddate',
            'u.mail',
            'u.photo'
        ],
        [
            'messageId',
            'userrId',
            'conversationId',
            'description',
            'senddate',
            'userMail',
            'userPhoto'
        ],
        ['m.conversationId', 'm.status'],
        [args.conversationId.value, 0],
        ['senddate']);

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));
}

exports.deleteMessageById = function (args, res, next) {
    /**
     * parameters expected in the args:
     * messageId (Long)
     **/
    // no response value expected for this operation
    res.end();
}

exports.getMessageById = function (args, res, next) {
    /**
     * parameters expected in the args:
     * messageId (Long)
     **/
    var examples = {};
    examples['application/json'] = {
        "conversationId": 1,
        "senddate": "aeiou",
        "messageId": 1,
        "description": "aeiou",
        "userId": 27,
        "status": 0
    };
    if (Object.keys(examples).length > 0) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
    } else {
        res.end();
    }

}

exports.updateMessage = function (args, res, next) {
    /**
     * parameters expected in the args:
     * message (Message)
     **/
    // no response value expected for this operation
    res.end();
}

