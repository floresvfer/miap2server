'use strict';

var url = require('url');


var Invoice = require('./InvoiceService');


module.exports.addInvoice = function addInvoice (req, res, next) {
  Invoice.addInvoice(req.swagger.params, res, next);
};

module.exports.addInvoiceP = function addInvoiceP (req, res, next) {
  Invoice.addInvoiceP(req.swagger.params, res, next);
};

module.exports.allInvoices = function allInvoices (req, res, next) {
  Invoice.allInvoices(req.swagger.params, res, next);
};

module.exports.allInvoicesStatus = function allInvoicesStatus (req, res, next) {
  Invoice.allInvoicesStatus(req.swagger.params, res, next);
};

module.exports.deleteInvoiceById = function deleteInvoiceById (req, res, next) {
  Invoice.deleteInvoiceById(req.swagger.params, res, next);
};

module.exports.getInvoiceById = function getInvoiceById (req, res, next) {
  Invoice.getInvoiceById(req.swagger.params, res, next);
};

module.exports.putInvoice = function putInvoice (req, res, next) {
  Invoice.putInvoice(req.swagger.params, res, next);
};
