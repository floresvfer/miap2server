'use strict';

var url = require('url');


var User = require('./UserService');


module.exports.addUser = function addUser (req, res, next) {
  User.addUser(req.swagger.params, res, next);
};

module.exports.addUserP = function addUserP (req, res, next) {
  User.addUserP(req.swagger.params, res, next);
};

module.exports.allUsers = function allUsers (req, res, next) {
  User.allUsers(req.swagger.params, res, next);
};

module.exports.allUserss = function allUserss (req, res, next) {
  User.allUserss(req.swagger.params, res, next);
};

module.exports.deleteUserById = function deleteUserById (req, res, next) {
  User.deleteUserById(req.swagger.params, res, next);
};

module.exports.getUserById = function getUserById (req, res, next) {
  User.getUserById(req.swagger.params, res, next);
};

module.exports.logIn = function logIn (req, res, next) {
  User.logIn(req.swagger.params, res, next);
};

module.exports.putUserP = function putUserP (req, res, next) {
  User.putUserP(req.swagger.params, res, next);
};

module.exports.validateUser = function validateUser (req, res, next) {
  User.validateUser(req.swagger.params, res, next);
};
