'use strict';

var url = require('url');


var Session = require('./SessionService');


module.exports.addSession = function addSession (req, res, next) {
  Session.addSession(req.swagger.params, res, next, req);
};

module.exports.addSessionP = function addSessionP (req, res, next) {
  Session.addSessionP(req.swagger.params, res, next, req);
};

module.exports.delSession = function delSession (req, res, next) {
  Session.delSession(req.swagger.params, res, next, req);
};

module.exports.getSession = function getSession (req, res, next) {
  Session.getSession(req.swagger.params, res, next, req);
};

module.exports.setSessionTest = function setSessionTest (req, res, next) {
  Session.setSessionTest(req.swagger.params, res, next, req);
};
