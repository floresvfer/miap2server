'use strict';

exports.addCart = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cart (Cart)
  **/
  // no response value expected for this operation
  res.end();
}

exports.addCartP = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cart (Cart)
  **/
  // no response value expected for this operation
  res.end();
}

exports.allCarts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "cartId" : 1,
  "createdate" : "2018-01-25",
  "userId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.allCartsStatus = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (Long)
  * status (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "cartId" : 1,
  "createdate" : "2018-01-25",
  "userId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.deleteCartById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cartId (Long)
  **/
  // no response value expected for this operation
  res.end();
}

exports.getCartById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cartId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "cartId" : 1,
  "createdate" : "2018-01-25",
  "userId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.putCart = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cart (Cart)
  **/
  // no response value expected for this operation
  res.end();
}

