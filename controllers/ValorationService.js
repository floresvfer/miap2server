'use strict';
const db = require('../utils/db');

exports.allValorations = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * status (Long)
  **/
  let ans = await
  db.Select(
      'valoration',
      ['valorationId',
                'description',
                'status'],
      ['status'],
      [args.status.value]
  );

  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
         ans || {},
         null,
         2));
};

exports.getValorationById = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * valorationId (Long)
  **/
  let ans = await
  db.Select(
      'valoration',
      ['valorationId',
                'description',
                'status'],
      ['valorationId'],
      [args.valorationId.value]
  );

  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));
};

