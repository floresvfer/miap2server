'use strict';
const db = require('../utils/db');

exports.addConversation = function (args, res, next) {
    /**
     * parameters expected in the args:
     * conversation (Conversation)
     **/
    // no response value expected for this operation
    res.end();
}

exports.addConversationP = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * conversation (Conversation)
     **/
    // no response value expected for this operation

    res.setHeader('Content-Type', 'application/json');
    res.end();
}

exports.allConversations = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * userId (Long)
     **/
    var examples = {};
    examples['application/json'] = {
        "converstionId": 1,
        "valoration": 5,
        "querydate": "aeiou",
        "userHelpedId": 27,
        "userHelperId": 27,
        "status": 0
    };

    let user = await db.Select2('userr',
        [
            'userTypeId'
        ], [
            'userrId'
        ], [
            args.userId.value
        ]);

    user = user[0];
    console.log(user.userTypeId);
    let ans;

    if(user.userTypeId === 1){
        ans = await db.SelectCustum('conversation c inner join userr u on c.userrHelpedId = u.userrId' +
            '                                             inner join userr u2 on c.userrHelperId = u2.userrId',
            [
                'c.conversationId',
                'c.userrHelpedId',
                'c.userrHelperId',
                'c.querydate',
                'c.valoration',
                'u.mail',
                'u2.mail'
            ],
            [
                'conversationId',
                'userHelpedId',
                'userHelperId',
                'querydate',
                'valoration',
                'userMail',
                'userMail2'
            ],
            [],
            [],
            ['querydate DESC']);
    }else {
        ans = await db.SelectCustum('conversation c inner join userr u on c.userrHelpedId = u.userrId',
            [
                'c.conversationId',
                'c.userrHelpedId',
                'c.userrHelperId',
                'c.querydate',
                'c.valoration',
                'u.mail'
            ],
            [
                'conversationId',
                'userHelpedId',
                'userHelperId',
                'querydate',
                'valoration',
                'userMail'
            ],
            ['userrHelperId',
                'c.status'],
            [args.userId.value,
                0],
            ['querydate DESC']);
    }

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));

}

exports.allConversationsStatus = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * userId (Long)
     * status (Long)
     **/
    var examples = {};
    examples['application/json'] = {
        "converstionId": 1,
        "valoration": 5,
        "querydate": "aeiou",
        "userHelpedId": 27,
        "userHelperId": 27,
        "status": 0
    };

    let user = await db.Select2('userr',
        [
            'userTypeId'
        ], [
            'userrId'
        ], [
            args.userId.value
        ]);

    console.log(user.userTypeId);

    let ans = await db.SelectCustum('conversation',
        [
            'conversationId',
            'userrHelpedId',
            'userrHelperId',
            'querydate',
            'valoration'
        ],
        [
            'conversationId',
            'userHelpedId',
            'userHelperId',
            'querydate',
            'valoration'
        ],
        ['userrHelpedId',
            'status'],
        [args.userId.value,
            args.status.value],
        ['querydate']);

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));

}

exports.deleteConversationById = function (args, res, next) {
    /**
     * parameters expected in the args:
     * conversationId (Long)
     **/
    // no response value expected for this operation
    res.end();
}

exports.getConversationById = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * conversationId (Long)
     **/
    var examples = {};
    examples['application/json'] = {
        "valoration": 5,
        "productId": 1,
        "commentId": 1,
        "publishdate": "2019-04-19",
        "description": "blah blah blah blah blah blha.....",
        "title": "mi titulo",
        "userId": 1,
        "status": 0
    };
    let ans = await db.SelectCustum('conversation',
        [
            'conversationId',
            'userrHelpedId',
            'userrHelperId',
            'querydate',
            'valoration',
            'status'
        ],
        [
            'conversationId',
            'userHelpedId',
            'userHelperId',
            'querydate',
            'valoration',
            'status'
        ],
        ['conversationId',
            'status'],
        [args.conversationId.value,
            0],
        []);

    ans = ans[0];

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));
}

exports.getConversationByUserId = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * userId (Long)
     **/
    var examples = {};
    examples['application/json'] = {
        "conversationId": 1,
        "valoration": 5,
        "querydate": "aeiou",
        "userHelpedId": 27,
        "userHelperId": 27,
        "status": 0
    };

    let ans = await db.SelectCustum('conversation',
        [
            'conversationId',
            'userrHelpedId',
            'userrHelperId',
            'querydate',
            'valoration',
            'status'
        ],
        [
            'conversationId',
            'userHelpedId',
            'userHelperId',
            'querydate',
            'valoration',
            'status'
        ],
        ['userrHelpedId',
            'status'],
        [args.userId.value,
            0],
        []);

    if (ans.length === 0) {
        let dateNow = new Date(Date.now());
        dateNow = dateNow.toISOString().slice(0, 19).replace('T', ' ');
        let helper = await db.getHelper();
        console.log(helper);
        if(helper.length !== 0) {
            helper = helper[0];
        }else{
            helper = {};
            helper.userId = 27;
        }
        await db.Insert('conversation',
            [
                'userrHelpedId',
                'userrHelperId',
                'querydate',
            ],
            [
                args.userId.value,
                helper.userId,
                dateNow
            ]
        );

        ans = await db.SelectCustum('conversation',
            [
                'conversationId',
                'userrHelpedId',
                'userrHelperId',
                'querydate',
                'valoration',
                'status'
            ],
            [
                'conversationId',
                'userHelpedId',
                'userHelperId',
                'querydate',
                'valoration',
                'status'
            ],
            ['userrHelpedId',
                'status'],
            [args.userId.value,
                0],
            []);
    }

    ans = ans[0];

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));

}

exports.updateConversation = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * conversation (Conversation)
     **/
        // no response value expected for this operation
    let conversation = args.conversation.value;
    let ans = await db.Update('conversation',
        [
            'valoration',
            'status'
        ],
        [
            conversation.valoration,
            1
        ],
        ['conversationId'],
        [conversation.conversationId]);

    console.log(conversation);

    res.setHeader('Content-Type', 'application/json');
    res.end();
}

