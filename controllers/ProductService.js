'use strict';
const db = require('../utils/db');

exports.addProduct = function(args, res, next) {
  /**
   * parameters expected in the args:
  * product (Product)
  **/
  // no response value expected for this operation
  res.end();
}

exports.addProductP = function(args, res, next) {
  /**
   * parameters expected in the args:
  * product (Product)
  **/
  // no response value expected for this operation
  let product = args.product.value;
  db.Insert('product',
      [
          'categoryId',
          'userrId',
          'description',
          'price',
          'publishdate',
          'amount',
          'name',
          'code'
      ],
      [
          product.category.categoryId,
          product.user.userId,
          product.description,
          product.price,
          product.publisdate,
          product.amount,
          product.name,
          product.code
      ]);
  res.end();
}

exports.allProducts = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * status (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "amount" : 10,
  "code" : "Y89YY3RRR",
  "productId" : 1,
  "price" : 50.0,
  "name" : "nombre del producto",
  "photo" : "photo.png",
  "description" : "descripcion del producto blah blah blah blah blah blah",
  "publisdate" : "2019-04-19",
  "category" : {
    "description" : "categoria",
    "categoryId" : 1,
    "categoryFather" : "",
    "status" : 0
  },
  "user" : {
    "gainObtained" : 400.0,
    "birthdate" : "2018-01-25",
    "address" : "29 av, 26-12 Z7",
    "availableCredit" : 5000.0,
    "gender" : {
      "genderId" : 0,
      "description" : "Masculino",
      "status" : 0
    },
    "mail" : "mymail@gmail.com",
    "photo" : "/home/photos/photo.jpg",
    "validateCode" : "f4MtvVAfXO2nnZZCeEnTGDpImEJ5MqVA",
    "registerdate" : "2018-01-25",
    "userId" : 1,
    "lastname" : "flores",
    "password" : "pass",
    "phone" : "54252919",
    "name" : "fer",
    "userType" : {
      "userTypeId" : 0,
      "description" : "admin",
      "status" : 0
    },
    "status" : 0
  },
  "status" : 0
};
  let ans = await db.getProducts(args.status.value);

  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));
  
}

exports.allProductss = async function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    var examples = {};
  examples['application/json'] = {
  "amount" : 10,
  "code" : "Y89YY3RRR",
  "productId" : 1,
  "price" : 50.0,
  "name" : "nombre del producto",
  "photo" : "photo.png",
  "description" : "descripcion del producto blah blah blah blah blah blah",
  "publisdate" : "2019-04-19",
  "category" : {
    "description" : "categoria",
    "categoryId" : 1,
    "categoryFather" : "",
    "status" : 0
  },
  "user" : {
    "gainObtained" : 400.0,
    "birthdate" : "2018-01-25",
    "address" : "29 av, 26-12 Z7",
    "availableCredit" : 5000.0,
    "gender" : {
      "genderId" : 0,
      "description" : "Masculino",
      "status" : 0
    },
    "mail" : "mymail@gmail.com",
    "photo" : "/home/photos/photo.jpg",
    "validateCode" : "f4MtvVAfXO2nnZZCeEnTGDpImEJ5MqVA",
    "registerdate" : "2018-01-25",
    "userId" : 1,
    "lastname" : "flores",
    "password" : "pass",
    "phone" : "54252919",
    "name" : "fer",
    "userType" : {
      "userTypeId" : 0,
      "description" : "admin",
      "status" : 0
    },
    "status" : 0
  },
  "status" : 0
};
  let ans = await db.SelectCustum('product p inner join category c on p.categoryId = c.categoryId',
      [
        'p.productId',
        'p.categoryId',
        'p.userrId',
        'p.photo',
        'p.description',
        'p.price',
        'p.publishdate',
        'p.amount',
        'p.status',
        'p.name',
        'p.code',
        'c.description'
      ],
      [
        'productId',
        'categoryId',
        'userrId',
        'photo',
        'description',
        'price',
        'publishdate',
        'amount',
        'status',
        'name',
        'code',
        'categoryDesc'
      ],
      ['p.status'],
      [0],
      ['p.publishdate']);

  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));
  
}

exports.deleteProductById = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * productId (Long)
  **/
  // no response value expected for this operation
  let ans = await db.Update('product',
      ['status'],
      [1],
      ['productId'],
      [args.productId.value]);

  res.end();
}

exports.getProductById = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * productId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "amount" : 10,
  "code" : "Y89YY3RRR",
  "productId" : 1,
  "price" : 50.0,
  "name" : "nombre del producto",
  "photo" : "photo.png",
  "description" : "descripcion del producto blah blah blah blah blah blah",
  "publisdate" : "2019-04-19",
  "category" : {
    "description" : "categoria",
    "categoryId" : 1,
    "categoryFather" : "",
    "status" : 0
  },
  "user" : {
    "gainObtained" : 400.0,
    "birthdate" : "2018-01-25",
    "address" : "29 av, 26-12 Z7",
    "availableCredit" : 5000.0,
    "gender" : {
      "genderId" : 0,
      "description" : "Masculino",
      "status" : 0
    },
    "mail" : "mymail@gmail.com",
    "photo" : "/home/photos/photo.jpg",
    "validateCode" : "f4MtvVAfXO2nnZZCeEnTGDpImEJ5MqVA",
    "registerdate" : "2018-01-25",
    "userId" : 1,
    "lastname" : "flores",
    "password" : "pass",
    "phone" : "54252919",
    "name" : "fer",
    "userType" : {
      "userTypeId" : 0,
      "description" : "admin",
      "status" : 0
    },
    "status" : 0
  },
  "status" : 0
};
  let ans = await db.SelectCustum('product p inner join category c on p.categoryId = c.categoryId',
      [
        'p.productId',
        'p.categoryId',
        'p.userrId',
        'p.photo',
        'p.description',
        'p.price',
        'p.publishdate',
        'p.amount',
        'p.status',
        'p.name',
        'p.code',
        'c.description'
      ],
      [
        'productId',
        'categoryId',
        'userrId',
        'photo',
        'description',
        'price',
        'publishdate',
        'amount',
        'status',
        'name',
        'code',
        'categoryDesc'
      ],
      ['productId'],
      [args.productId.value],
      []);

  ans = ans[0];

  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));
  
}

exports.updateProduct = function(args, res, next) {
  /**
   * parameters expected in the args:
  * product (Product)
  **/
  // no response value expected for this operation
  res.end();
}

