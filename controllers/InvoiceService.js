'use strict';

exports.addInvoice = function(args, res, next) {
  /**
   * parameters expected in the args:
  * invoice (Invoice)
  **/
  // no response value expected for this operation
  res.end();
}

exports.addInvoiceP = function(args, res, next) {
  /**
   * parameters expected in the args:
  * invoice (Invoice)
  **/
  // no response value expected for this operation
  res.end();
}

exports.allInvoices = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : "2019-04-19",
  "purchaseId" : 1,
  "invoiceId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.allInvoicesStatus = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (Long)
  * status (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : "2019-04-19",
  "purchaseId" : 1,
  "invoiceId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.deleteInvoiceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * invoiceId (Long)
  **/
  // no response value expected for this operation
  res.end();
}

exports.getInvoiceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * invoiceId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : "2019-04-19",
  "purchaseId" : 1,
  "invoiceId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.putInvoice = function(args, res, next) {
  /**
   * parameters expected in the args:
  * invoice (Invoice)
  **/
  // no response value expected for this operation
  res.end();
}

