'use strict';

exports.addCartDetail = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cartDetail (CartDetail)
  **/
  // no response value expected for this operation
  res.end();
}

exports.addCartDetailP = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cartDetail (CartDetail)
  **/
  // no response value expected for this operation
  res.end();
}

exports.allCartDetails = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cartId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "amount" : 1,
  "productId" : 1,
  "cartId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.deleteCartDetailById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cartDetailId (Long)
  **/
  // no response value expected for this operation
  res.end();
}

exports.getCartDetailById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cartDetailId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "amount" : 1,
  "productId" : 1,
  "cartId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.updateCartDetail = function(args, res, next) {
  /**
   * parameters expected in the args:
  * cartDetail (CartDetail)
  **/
  // no response value expected for this operation
  res.end();
}

