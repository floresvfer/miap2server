'use strict';

var url = require('url');


var PurchaseDetail = require('./PurchaseDetailService');


module.exports.addPurchaseDetail = function addPurchaseDetail (req, res, next) {
  PurchaseDetail.addPurchaseDetail(req.swagger.params, res, next);
};

module.exports.addPurchaseDetailP = function addPurchaseDetailP (req, res, next) {
  PurchaseDetail.addPurchaseDetailP(req.swagger.params, res, next);
};

module.exports.allPurchaseDetails = function allPurchaseDetails (req, res, next) {
  PurchaseDetail.allPurchaseDetails(req.swagger.params, res, next);
};

module.exports.deletePurchaseDetailById = function deletePurchaseDetailById (req, res, next) {
  PurchaseDetail.deletePurchaseDetailById(req.swagger.params, res, next);
};

module.exports.getPurchaseDetailById = function getPurchaseDetailById (req, res, next) {
  PurchaseDetail.getPurchaseDetailById(req.swagger.params, res, next);
};

module.exports.updatePurchaseDetail = function updatePurchaseDetail (req, res, next) {
  PurchaseDetail.updatePurchaseDetail(req.swagger.params, res, next);
};
