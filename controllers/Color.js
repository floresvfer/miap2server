'use strict';

var url = require('url');


var Color = require('./ColorService');


module.exports.allColors = function allColors (req, res, next) {
  Color.allColors(req.swagger.params, res, next);
};

module.exports.getColorrById = function getColorrById (req, res, next) {
  Color.getColorrById(req.swagger.params, res, next);
};
