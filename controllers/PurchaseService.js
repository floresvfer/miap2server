'use strict';

exports.addPurchase = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchase (Purchase)
  **/
  // no response value expected for this operation
  res.end();
}

exports.addPurchaseP = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchase (Purchase)
  **/
  // no response value expected for this operation
  res.end();
}

exports.allPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "purchaseId" : 1,
  "userId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.allPurchasesStatus = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (Long)
  * status (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "purchaseId" : 1,
  "userId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.deletePurchaseById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchaseId (Long)
  **/
  // no response value expected for this operation
  res.end();
}

exports.getPurcahseById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchaseId (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "purchaseId" : 1,
  "userId" : 1,
  "status" : 0
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.putPurchase = function(args, res, next) {
  /**
   * parameters expected in the args:
  * purchase (Purchase)
  **/
  // no response value expected for this operation
  res.end();
}

