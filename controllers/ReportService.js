'use strict';
const pdf = require('html-pdf');
const phantom = require('phantom');
const fs = require('fs');
const reportBase = require('../utils/reportBase');

exports.getReport = async function(args, res, next) {
  /**
   * parameters expected in the args:
   * reportId (Long)
   * x (String)
   * y (String)
   **/
      // no response value expected for this operation
  let id = args.reportId.value;
  let x = args.x.value;
  let y = args.y.value;
  let html = await reportBase.getDoc(id, x, y);
  await pdf.create(html).toStream(function(err, stream){
    stream.pipe(fs.createWriteStream('reports/foo.pdf'));
    stream.on('close', function () {
      let readStream = fs.createReadStream('reports/foo.pdf');
      // We replaced all the event handlers with a simple call to readStream.pipe()
      readStream.pipe(res);
    })
  });
}

exports.getReport2 = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * reportId (Long)
  * x (String)
  * y (String)
  **/
  // no response value expected for this operation
  let id = args.reportId.value;
  let x = args.x.value;
  let y = args.y.value;
  let ans = await reportBase.getJSON(id, x, y);
  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));
  res.end();
}

