'use strict';

var url = require('url');


var Cart = require('./CartService');


module.exports.addCart = function addCart (req, res, next) {
  Cart.addCart(req.swagger.params, res, next);
};

module.exports.addCartP = function addCartP (req, res, next) {
  Cart.addCartP(req.swagger.params, res, next);
};

module.exports.allCarts = function allCarts (req, res, next) {
  Cart.allCarts(req.swagger.params, res, next);
};

module.exports.allCartsStatus = function allCartsStatus (req, res, next) {
  Cart.allCartsStatus(req.swagger.params, res, next);
};

module.exports.deleteCartById = function deleteCartById (req, res, next) {
  Cart.deleteCartById(req.swagger.params, res, next);
};

module.exports.getCartById = function getCartById (req, res, next) {
  Cart.getCartById(req.swagger.params, res, next);
};

module.exports.putCart = function putCart (req, res, next) {
  Cart.putCart(req.swagger.params, res, next);
};
