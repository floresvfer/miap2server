'use strict';
const db = require('../utils/db');

exports.allUserTypes = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * status (Long)
  **/
  let ans = await
  db.Select(
      'userType',
      ['userTypeId',
                'description',
                'status'],
      ['status'],
      [args.status.value]
  );

  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));

};

exports.getUserTypeById = async function(args, res, next) {
  /**
   * parameters expected in the args:
  * userTypeId (Long)
  **/
  let ans = await
      db.Select(
          'userType',
          ['userTypeId',
            'description',
            'status'],
          ['userTypeId'],
          [args.userTypeId.value]
      );

  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          ans || {},
          null,
          2));
  
};

