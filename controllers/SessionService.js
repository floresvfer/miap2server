'use strict';

exports.addSession = function(args, res, next, req) {
  /**
   * parameters expected in the args:
  * session (Session)
  **/
  // no response value expected for this operation

  res.end();
}

exports.addSessionP = function(args, res, next, req) {
  /**
   * parameters expected in the args:
  * session (Session)
  **/
  // no response value expected for this operation
  req.session.session = args.session.value;
  req.session.save();
  console.log(req.session.session);
  res.end();
}

exports.delSession = function(args, res, next, req) {
  /**
   * parameters expected in the args:
  **/
  // no response value expected for this operation

  req.session.destroy();
  res.end();
}

exports.getSession = function(args, res, next, req) {
  /**
   * parameters expected in the args:
  **/
  // no response value expected for this operation
  console.log(req.session.usr);
  res.setHeader('Content-Type', 'application/json');
  res.end(
      JSON.stringify(
          req.session.usr || {},
          null,
          2));
  res.end();
}

exports.setSessionTest = function(args, res, next, req) {
  req.session.usr = {
    userrId: args.userrId.value,
    userTypeId: args.userTypeId.value,
    genderId: args.genderId.value,
    photo: args.photo.value,
    name: args.name.value,
    mail: args.mail.value
  };
  console.log(req.session);
  res.end(
      JSON.stringify(
          req.session.usr || {},
          null,
          2));
  res.end();
}

