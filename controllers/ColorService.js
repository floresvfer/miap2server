'use strict';
const db = require('../utils/db');

exports.allColors = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * status (Long)
     **/
    let ans = await
        db.Select('color',
            ['colorId',
                'description',
                'status'],
            ['status'],
            [args.status.value]);

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));

};

exports.getColorrById = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * colorId (Long)
     **/
    let ans = await
        db.Select(
            'color',
            ['colorId',
                'description',
                'status'],
            ['colorId'],
            [args.colorId.value]
        );

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));

};

