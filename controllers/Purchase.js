'use strict';

var url = require('url');


var Purchase = require('./PurchaseService');


module.exports.addPurchase = function addPurchase (req, res, next) {
  Purchase.addPurchase(req.swagger.params, res, next);
};

module.exports.addPurchaseP = function addPurchaseP (req, res, next) {
  Purchase.addPurchaseP(req.swagger.params, res, next);
};

module.exports.allPurchases = function allPurchases (req, res, next) {
  Purchase.allPurchases(req.swagger.params, res, next);
};

module.exports.allPurchasesStatus = function allPurchasesStatus (req, res, next) {
  Purchase.allPurchasesStatus(req.swagger.params, res, next);
};

module.exports.deletePurchaseById = function deletePurchaseById (req, res, next) {
  Purchase.deletePurchaseById(req.swagger.params, res, next);
};

module.exports.getPurcahseById = function getPurcahseById (req, res, next) {
  Purchase.getPurcahseById(req.swagger.params, res, next);
};

module.exports.putPurchase = function putPurchase (req, res, next) {
  Purchase.putPurchase(req.swagger.params, res, next);
};
