'use strict';
const db = require('../utils/db');

exports.allGenders = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * status (Long)
     **/
    let ans = await db.Select('gender',
        ['genderId',
            'description',
            'status'],
        ['status'],
        [args.status.value]);


    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));
};

exports.getGenderById = async function (args, res, next) {
    /**
     * parameters expected in the args:
     * genderId (Long)
     **/
    let ans = await db.Select('gender',
        ['genderId',
            'description',
            'status'],
        ['genderId'],
        [args.genderId.value]);

    res.setHeader('Content-Type', 'application/json');
    res.end(
        JSON.stringify(
            ans || {},
            null,
            2));
};

