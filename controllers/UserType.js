'use strict';

var url = require('url');


var UserType = require('./UserTypeService');


module.exports.allUserTypes = function allUserTypes (req, res, next) {
  UserType.allUserTypes(req.swagger.params, res, next);
};

module.exports.getUserTypeById = function getUserTypeById (req, res, next) {
  UserType.getUserTypeById(req.swagger.params, res, next);
};
